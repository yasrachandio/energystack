/**
 * Interface for Source, Sending energy works from this interface.
 *
 * @author Yasra Chandio & Qasim Raza
 * @see SourceP for implemenetaation
 *
 * 
 */
 
 interface Source
              {
		// Command to send the energy  
            	command void energysend(uint16_t NodeID, int Time_Energy_Duration);
    
		//event to signal when energy is transferred to node            
          	event void energy_done();


             }
