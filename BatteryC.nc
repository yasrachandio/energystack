/*
* Author: Yasra Chandio
* Date : March-2015
* Configuration file of Battery interface @see BatteryP, Battery interface
/

configuration BatteryC
{
  	provides interface Battery;
        provides interface BatteryApplication;
	
}

implementation
 {
        components BatteryP;
        Battery = BatteryP;
        BatteryApplication = BatteryP;
	
}