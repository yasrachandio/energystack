	/*
	*  Configuration file of Source interface
	* 
	*
	* @author: Yasra & Qasim
    * @see SourceP, Source 
	*/

	configuration SourceC
	{
		provides interface Source;
		
	}

	implementation
	 {
		components SourceP,Network_ModelC,SourceParametersC;

		Source = SourceP;

		SourceP.NetworkModelSource -> Network_ModelC.NetworkModelSource;
		SourceP.SourceParameters -> SourceParametersC.SourceParameters;
	}
