/**
 * The implementation of the Network Model interfaces.
 *
 * @author Yasra 
 * 
 *
 * 
 */
// header file for energy

#include <AM.h>
#include <sim_tossim.h>
#include <sim_event_queue.h>
#include <heap.h>
#include <message.h>
#include <Atm128Timer.h>
#include <hardware.h>
#include <platform_hardware.h>

// int time-check;


module Network_ModelP @safe()
{
  provides
  {
   interface NetworkModelSource;     // Interface for interacting with source
   interface NetworkModelHarvester;  // Interface for Interacting with Harvest
  } 
 uses 
{
interface NetworkLoss;
}
 }


implementation 
  {

  sim_time_t timeinmilli;
  sim_time_t avr_time;
  sim_time_t temp;
  int Distance;
  char MediumType;
  uint16_t NodeIDno;
  int AmountofEnergy;  // variable to store amount of energy after calculating network loss

      void energy_harvest_receive_handle(sim_event_t* e)   // event handler for harvest energy event queue
            {
               
               int EnergyTransferAN = e->energy_transfer; // Energy transfer after network
               int EnergyTransferT = e->energy_time;    // Energy transfer duration
               int SourceT = e->type_source;           // source type
               dbg("Network_Model", "Event-Queue works");
               if (TOS_NODE_ID != 1 ) 
                    {
          signal NetworkModelHarvester.HarvestDone(EnergyTransferAN ,EnergyTransferT,SourceT); // this event is called when mote is not source 
                    }
               
            } 
			
          // function to insert the event in queue
        void sim_harvest_energy_event(uint16_t NodeID,int EnergyTransfer, int EnergyTransferTime,int TypeofSource)  
       {
       
            sim_event_t* evt = sim_queue_allocate_event();
            
            timeinmilli = EnergyTransferTime;
            avr_time= timeinmilli * 7372800ULL/1000;
            temp=(avr_time * sim_ticks_per_sec());
			temp /= 7372800ULL;			
	   
            evt->mote = NodeID;
            evt->time =sim_time()+ temp;
            evt->handle = energy_harvest_receive_handle; 
			evt->cleanup = sim_queue_cleanup_event;
            evt->cancelled = 0;
            evt->force = 1;
            evt->data = NULL;
            evt->energy_transfer = EnergyTransfer; // Energy transfer
            evt->type_source = TypeofSource; // Energy transfer
            evt->energy_time = EnergyTransferTime;
            sim_queue_insert(evt);

       }


     command void NetworkModelSource.EnergyTransferNetwork(uint16_t NodeID,int Time_Energy_Duration,int EnergyTransfer,int TypeofSource)
	{
                
    /* Here we will do the energy calculation depending on type of source
       and and insert event for harvest. First call the laser_Model for amount 
       of energy to be tranferred after computing any network-loss which is zero in laser case */
                
          
   /////////////// calculate loss in energy due to type of source ////////////////////////////////

            if (TypeofSource == 1)  // for Laser
            {
			AmountofEnergy =  call NetworkLoss.AmountEnergyAfterLaserNetworkLoss(NodeID,Time_Energy_Duration,EnergyTransfer);
////////////////////  calculate distance between Nodes of soruce and harvester/////////////////////
			Distance = call NetworkLoss.DistanceBetweenNodes(NodeID);
			dbg("Network_Model", "Distance Between Source and Node is %du\n",Distance);
            
//////////////////// Find type of medium between source and harvester/////////////////////////////////

			MediumType = call NetworkLoss.TypeofMedium();
			sim_harvest_energy_event(NodeID,AmountofEnergy,Time_Energy_Duration,TypeofSource);  // call function to insert event in the queuu
			signal NetworkModelSource.EnergyTransferNetworkDone(); // transfer done event  

      }
         if (TypeofSource == 2)  // for Radio       
       {
			AmountofEnergy =  call NetworkLoss.AmountEnergyAfterLaserNetworkLoss(NodeID,Time_Energy_Duration,EnergyTransfer);
            
////////////////////  calculate distance between Nodes of soruce and harvester/////////////////////

			Distance = call NetworkLoss.DistanceBetweenNodes(NodeID);
			dbg("Network_Model", "Distance Between Source and Node is %du\n",Distance);
            
//////////////////// Find type of medium between source and harvester/////////////////////////////////

			MediumType = call NetworkLoss.TypeofMedium();
			sim_harvest_energy_event(NodeID,AmountofEnergy,Time_Energy_Duration,TypeofSource);  // call function to insert event in the queuu
			signal NetworkModelSource.EnergyTransferNetworkDone(); // transfer done event   
         }
        
	}
    
// Implementation of NetworkLoss event
event void NetworkLoss.NetworkLossDone(int TransferAmountEnergy )
         {
                   
           dbg("Network_Model", "Network_loss_works");
                    
         }

  }


