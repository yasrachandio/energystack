/**
 * Implementation of the Energy interface.
 *
 * @author Yasra Chandio & Qasim Raza
 * 
 *
 * 
 */
 
#include <tos.h>

module SourceP @safe()
{
	provides 
	{
		interface Source;
	}

	uses 
	{
		interface Netwo	rkModelSource;
		interface SourceParameters;
	}

} 

implementation 
{ 	 
 int EnergyTransferLaser = 800; // mWfor Laser
 int EnergyTransferRadio = 24;  // dbm for radio
 int SourceType;
       
         // Sending energy from Master to leaf 
        command void Source.energysend(uint16_t NodeID, int Time_Energy_Duration)
	{          
			signal Source.energy_done(); 

			SourceType = call SourceParameters.TypeofSource();
			
			if (SourceType == 1)
				{
				call  NetworkModelSource.EnergyTransferNetwork(NodeID,Time_Energy_Duration,EnergyTransferLaser,1);
				}
			if (SourceType ==2)
				{
				EnergyTransferRadio = call SourceParameters.RadioSourceCableLoss(EnergyTransferRadio,2,100);
				EnergyTransferRadio = call SourceParameters.RadioSourceAnteenaGain(EnergyTransferRadio,2);
				call  NetworkModelSource.EnergyTransferNetwork(NodeID,Time_Energy_Duration,EnergyTransferRadio,2);     
				}
         
	}
         
	  

        

event void NetworkModelSource.EnergyTransferNetworkDone()
		{	         
                // could be use later
        }

	
  }

